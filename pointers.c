#include<stdio.h>
 void swap(int *num1,int *num2)
 {
  int temp;
  temp=*num1;
  *num1=*num2;
  *num2=temp;
 }
int main()
{
 int num1,num2;
 printf("enter any two numbers\n");
 scanf("%d %d",&num1,&num2);
 printf("the numbers you entered is:\n%d %d\n",num1,num2);
 swap(&num1,&num2);
 printf("the numbers after swapping is:\n%d %d\n",num1,num2);
 return 0;
}